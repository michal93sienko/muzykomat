import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;

import javax.swing.*;

public class SwingControlDemo {

   private JFrame mainFrame;
   static JButton tabBut[];
   public static  int nRow;// = 15;
   public static int nCol;// = 15;
   private static int size = 30;
   private static int rowScale[];
   private static int colScale[];
   public static Integer liczba;
   private static byte matrix[];
   private static byte workMatrix[];
   private static final byte [] toUp = {1,3,5,7,9,11,13,15};
   private static final byte [] toDown = {2,3,6,7,10,11,14,15};
   private static final byte [] toLeft = {8,9,10,11,12,13,14,15};
   private static final byte [] toRight = {4,5,6,7,12,13,14,15};
   private static boolean WORK_MATRIX_IS_ACTUAL;
   private static boolean PLAYED;
   public static final byte UP=1;
   public static final byte DOWN=2;
   public static final byte RIGHT=4;
   public static final byte LEFT=8;
   private short k;
   private static List<Integer> currentVoices = new ArrayList<>();
   private static Scanner sc;
   private static SwingControlDemo swingControlDemo;
   private static long start,stop;
/////***********IMPORTANT***********//
//jesli wprowadzasz punkty startu w miejscach 'obrzeznych'
//musisz ich kierunek ustalić DO krawedzi. 
//else
//	bedzie zle!
//////***********************************//

	private static class Voice extends Thread {
		int k;
		public Voice() {
			// TODO Auto-generated constructor stub
		}

		 private boolean isInByteArray(byte x, byte [] tab){
			   for(k = 0;k<tab.length;k++)
				   if(tab[k] == x) return true;
			   return false;
		   }

		@Override
		public void run() {
			while (true) {
				if (WORK_MATRIX_IS_ACTUAL){//nie musi nic robic
					try {
						sleep(300);
						//System.out.println("czekam na maina");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					currentVoices.clear();

					for (int i = 0; i < nCol * nRow; i++) {
						if (matrix[i] != 0) {// omija puste komorki
							if (i % nCol > 0 && i % nCol < nCol - 1 && i > nCol
									&& i < nCol * nRow - 1 - nCol) { // nie po
																		// brzegach!
								if (isInByteArray(matrix[i], toUp))
									workMatrix[i - nCol] += UP;
								if (isInByteArray(matrix[i], toDown))
									workMatrix[i + nCol] += DOWN;
								if (isInByteArray(matrix[i], toRight))
									workMatrix[i + 1] += RIGHT;
								if (isInByteArray(matrix[i], toLeft))
									workMatrix[i - 1] += LEFT;

							} else if (i == 0) {// lewy gorny rog
								if (matrix[i] == LEFT) {
									workMatrix[i + 1] += RIGHT;
									currentVoices.add(rowScale[i]);
								}
								if (matrix[i] == UP) {
									workMatrix[i + nCol] += DOWN;
									currentVoices.add(colScale[i]);
								}
								if (matrix[i] == (UP + LEFT)) {
									workMatrix[i + 1] += RIGHT;
									workMatrix[i + nCol] += DOWN;
									currentVoices.add(colScale[i]);
									currentVoices.add(rowScale[i]);
								}
								} else if (i == (nCol - 1)) {// prawy gorny rog
								if (matrix[i] == RIGHT) {
									workMatrix[i - 1] += LEFT;
									currentVoices.add(rowScale[0]);
								}
								if (matrix[i] == UP) {
									workMatrix[i + nCol] += DOWN;
									currentVoices.add(colScale[i]);
								}
								if (matrix[i] == RIGHT + UP) {
									workMatrix[i - 1] += LEFT;
									workMatrix[i + nCol] += DOWN;
									currentVoices.add(rowScale[i]);
									currentVoices.add(colScale[i]);
								}
								// isChange=true;
								// return true;
							} else if (i == nRow * nCol - nCol) {// lewy dolny
																	// rog
								
								if (matrix[i] == LEFT) {
									workMatrix[i + 1] += RIGHT;
									currentVoices.add(rowScale[i / nCol]);
								}
								if (matrix[i] == DOWN) {
									workMatrix[i - nCol] += UP;
									currentVoices.add(rowScale[i % nRow]);
								}
								if (matrix[i] == LEFT + DOWN) {
									workMatrix[i + 1] += RIGHT;
									workMatrix[i - nCol] += UP;
									currentVoices.add(rowScale[i % nRow]);
									currentVoices.add(rowScale[i / nCol]);
								}
								} else if (i == nRow * nCol - 1) {// prawy dolny rog
								if (matrix[i] == RIGHT) {
									workMatrix[i - 1] += LEFT;
									currentVoices.add(rowScale[i / nCol]);
								}
								if (matrix[i] == DOWN) {
									workMatrix[i - nCol] += UP;
									currentVoices.add(rowScale[i % nRow]);
								}
								if (matrix[i] == RIGHT + DOWN) {
									workMatrix[i - nCol] += UP;
									workMatrix[i - 1] += LEFT;
									currentVoices.add(rowScale[i / nCol]);
									currentVoices.add(rowScale[i % nRow]);
								}
								// isChange=true;
								// return true;
							} else if (i < nCol) { // pierwszy wiersz
								if (isInByteArray(matrix[i], toRight))
									workMatrix[i + 1] += RIGHT;
								if (isInByteArray(matrix[i], toLeft))
									workMatrix[i - 1] += LEFT;
								if (isInByteArray(matrix[i], toUp)) {
									workMatrix[i + nCol] += DOWN;
									currentVoices.add(rowScale[i % nRow]);
								}
								} else if (i > nCol * nRow - 1 - nCol) { // ostatni
																		// wiersz
								if (isInByteArray(matrix[i], toRight))
									workMatrix[i + 1] += RIGHT;
								if (isInByteArray(matrix[i], toLeft))
									workMatrix[i - 1] += LEFT;
								if (isInByteArray(matrix[i], toDown)) {
									workMatrix[i - nCol] += UP;
									currentVoices.add(rowScale[i % nRow]);
								}
								} else if (i % nCol == 0) { // pierwsza kolumna
								// if (toDown.contains((int)matrix[i-nCol]))
								// sum+=DOWN;
								if (isInByteArray(matrix[i], toUp))
									workMatrix[i - nCol] += UP;
								if (isInByteArray(matrix[i], toDown))
									workMatrix[i + nCol] += DOWN;
								if (isInByteArray(matrix[i], toLeft)) {
									workMatrix[i + 1] += RIGHT;
									currentVoices.add(rowScale[i / nCol]);
								}
								} else if (i % nCol == (nCol - 1)) { // ostatnia
																	// kolumna
								if (isInByteArray(matrix[i], toUp))
									workMatrix[i - nCol] += UP;
								if (isInByteArray(matrix[i], toDown))
									workMatrix[i + nCol] += DOWN;
								if (isInByteArray(matrix[i], toRight)) {
									workMatrix[i - 1] += LEFT;
									currentVoices.add(rowScale[i / nCol]);
								}
								}
						}
					}
					//COUNTING_FINISH=true;
					//System.out.println("zmienam flage na true");
					WORK_MATRIX_IS_ACTUAL=true;
				}
			}
		}
	}
 
   public SwingControlDemo(){
	   
		//
	}

   private void setRowScale(){
	   System.out.println("podaj "+nRow+" dzwiekow w odpowiedniej kolejnosci. 0 oznacza a1");
	   for(int i=0;i<nRow;i++)
		   rowScale[i]=sc.nextInt();
   }
   private void setColScale(){
	   System.out.println("podaj "+nRow+" dzwiekow w odpowiedniej kolejnosci. 0 oznacza a1");
	   for(int i=0;i<nCol;i++)
		   colScale[i]=sc.nextInt();
   }
	private void initialize() {
		sc = new Scanner(System.in);
		System.out.println("podaj liczbe wierszy: ");
		nRow=sc.nextInt();
		System.out.println("podaj liczbe kolumne: ");
		nCol=sc.nextInt();
		//nRow=15;nCol=15;
		colScale = new int[nCol];
		rowScale=new int[ nRow];
		
		tabBut = new JButton[nRow * nCol];
		matrix = new byte[nRow * nCol];
		workMatrix = new byte[nRow * nCol];
		prepareGUI();
		
		System.out
				.println("Ile pkt poczatkowych chcesz ustawic [liczba wierszy | liczba kolumn | kerunek]?\n1-UP 2-DOWN 4-LEFT 8-RIGHT\n");
		
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			System.out.println("wprowadzasz punkt " + (i + 1) + ": ");
			this.scanStartPoint();
		}
		System.out.println("wprowadzasz skale wierszy: ");
		setRowScale();
		System.out.println("wprowadzasz skale kolumn: ");
		setColScale();
	}

	public static void main(String[] args) throws InterruptedException {
		
		swingControlDemo = new SwingControlDemo();
		swingControlDemo.initialize();
		Voice v = new Voice();
		

		swingControlDemo.displayCurrentMatrix();
		WORK_MATRIX_IS_ACTUAL = true;
		v.start();

		Thread.sleep(2000);
		int i;
		PLAYED = false;

		while (true) {
			StdAudio.playInts(currentVoices);
			Thread.sleep(0);
			Arrays.fill(workMatrix, (byte) 0);
			WORK_MATRIX_IS_ACTUAL = false;
			currentVoices.clear();
			swingControlDemo.displayCurrentMatrix();
			while (!WORK_MATRIX_IS_ACTUAL)
				Thread.sleep(20);// czekamy na zakonczenie liczenia

			for (i = 0; i < nRow * nCol; i++)
				matrix[i] = workMatrix[i];
		}
	}
	
   private boolean isInByteArray(byte x, byte [] tab){
	   for(k = 0;k<tab.length;k++)
		   if(tab[k] == x) return true;
	   return false;
   }
	
	private void scanStartPoint(){
	   int a = sc.nextInt();
	   while(a<0 || a>=nRow){
		   System.out.println("bledny wiersz");
		   a = sc.nextInt();
	   }
	   int b = sc.nextInt();
	   while(b<0 || b>=nCol){
		   System.out.println("bledna kolumna");
		   b = sc.nextInt();
	   }
	   byte direct= sc.nextByte() ;
	   while(direct != 1 && direct != 2 && direct != 4 && direct != 8){
		   System.out.println("bledny kierunek"); 
		   direct = sc.nextByte();
	   }
	   setStartPoint(a, b, direct);
   }
   
   private void setStartPoint(int a, int b, byte direct){ // 0 <= a/b < nRow/nCol
	   matrix[a*nCol + b] = direct;
   }
   private void prepareGUI(){
      mainFrame = new JFrame("Muzykomat");
      mainFrame.setSize(nRow*size,nRow*size);
      mainFrame.setLayout(new GridLayout(nRow, nCol));
      mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

     
	   for(int i=0;i<nRow*nCol;i++){
		   tabBut[i]=new JButton();
		   //tabBut[i].setSize(size, size);
		   
			matrix[i] = 0;
		}
		//mainFrame.setVisible(true);
	}

	private void displayCurrentMatrix() {

		for (int i = 0; i < nRow * nCol; i++) {
			if (matrix[i] < 0)
				tabBut[i].setBackground(Color.red);
			else if (matrix[i] == 0)
				tabBut[i].setBackground(Color.blue);
			else
				tabBut[i].setBackground(Color.yellow);

			mainFrame.add(tabBut[i]);
		}
      mainFrame.setVisible(true);  
   }
}
